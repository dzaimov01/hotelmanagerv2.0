﻿using System;

namespace Common
{
    public static class ErrorMesseges
    {
        public const string negativeNumberErrorMessage = "Please enter a value bigger than 0";
        public const string doesntHave10CharactersErrorMessege = "This field must be 10 characters";
        public const string containsLettersErrorMessege = "This field must not contain characters";
        public const string containsNumbersErrorMessege = "This field must not contain letters";
    }
}
