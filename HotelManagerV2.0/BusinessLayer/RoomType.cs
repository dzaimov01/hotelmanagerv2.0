﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public enum RoomType
    {
        // RoomType = Capacity

        SingleBed = 1,
        TwoSingleBeds = 2,
        ThreeSingleBeds = 3,
        SingleAndDoubleBed = 3,
        SingleApartment = 1,
        DoubleApartment = 2,
        TripleApartment = 3,
        Penthouse = 5,
        Maisonette = 9
    }
}
